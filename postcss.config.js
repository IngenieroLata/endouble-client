 const postcssPresetEnv = require('postcss-preset-env');
 const tailwindCss = require('tailwindcss');

module.exports = {
  ident: 'postcss',
  plugins: [
    postcssPresetEnv(),
    tailwindCss('./tailwind.js'),
    require('autoprefixer')
  ]
}
