# Endouble-client

This is the solution for the assignment "The vacancy apply page" as part of the Technical test stage at [Endoble](https://endouble.com/).

## Getting Started

No special CLI or any type of black magic is needed, so go ahead and clone this repository.

### Prerequisites

Although we are going to need `Node.js (v8.0+ required)`

### Quick Start

Install dependencies

```
$ npm install
```

And run as development

```
$ npm start
```

Check out `http://localhost:8080`

## Built With

- [Webpack](https://webpack.js.org/) - Module bundler
- [React](https://reactjs.org/) - UI Javascript library
- [Redux](https://redux.js.org/) - Predictable state container
- [Tailwind CSS](https://tailwindcss.com/) - Utility-First CSS Framework
- [Font Awesome](https://fontawesome.com/) - Vector icons and social logos

Among others libraries, see the [package.json](package.json) file.

## Author

- **Diego Toro**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
