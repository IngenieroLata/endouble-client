/* Constants */
export const APPLY = 'endouble/job/APPLY';

const job = {
  id: 'ABC123',
  title: 'PURCHASING ASSISTANT',
  location: 'AMSTERDAM, THE NETHERLANDS',
  formFields: [
    {
      name: 'firstName',
      type: 'text',
      placeholder: 'First name',
      validation: ['required'],
    },
    {
      name: 'lastName',
      type: 'text',
      placeholder: 'Last name',
      validation: ['required'],
    },
    {
      name: 'birthDay',
      type: 'date',
      placeholder: 'Date of birth',
      validation: ['required'],
    },
    {
      name: 'email',
      type: 'email',
      placeholder: 'E-mail address',
      validation: ['required', 'email'],
    },
    {
      name: 'sex',
      type: 'select',
      placeholder: 'Choose your sex',
      values: [
        { value: 'female', label: 'Female' },
        { value: 'male', label: 'Male' },
        { value: 'other', label: 'Who Cares?' },
      ],
      validation: ['required'],
    },
    {
      name: 'address',
      type: 'text',
      placeholder: 'Address',
      validation: ['required'],
    },
    {
      name: 'houseNumber',
      type: 'text',
      placeholder: 'House number',
      validation: ['required'],
    },
    {
      name: 'zip',
      type: 'text',
      placeholder: 'Zipcode',
      validation: ['required'],
    },
  ],
};

/* Reducers */
export default function reducer(state = job, action) {
  switch (action.type) {
    case APPLY:
    default:
      return state;
  }
}

/* Action Creatores */
export const applyToJob = (id, formValues) => dispatch => {
  dispatch({ type: APPLY, payload: { id, formValues } });
};
