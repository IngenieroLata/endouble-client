/* Constants */
const navLinks = [
  { label: 'HOME', to: '/' },
  { label: 'JOBS', to: '/' },
  { label: 'ABOUT', to: '/' },
  { label: 'DEPARTMENTS', to: '/' },
  { label: 'CONTACT', to: '/' },
];

/* Reducers */
export default function reducer(state = navLinks) {
  return state;
}
