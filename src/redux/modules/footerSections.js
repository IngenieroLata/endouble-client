/* Constants */
const footerSections = [
  {
    title: 'JOBS AT',
    links: [
      { label: 'Home', to: '/' },
      { label: 'Jobs', to: '/' },
      { label: 'About', to: '/' },
      { label: 'Department', to: '/' },
    ],
  },
  {
    title: 'INFORMATION',
    links: [
      { label: 'F.A.Q.', to: '/' },
      { label: 'Application process', to: '/' },
      { label: 'Privacy policy', to: '/' },
      { label: 'Contact', to: '/' },
    ],
  },
  {
    title: 'FOLLOW US',
    sectionClass: 'xs:max-h-36',
    links: [
      { label: 'Newsletter', to: '/', icon: 'envelope' },
      { label: 'Instagram', to: '/', icon: ['fab', 'instagram'] },
      { label: 'Twitter', to: '/', icon: ['fab', 'twitter'] },
      { label: 'Tumblr', to: '/', icon: ['fab', 'tumblr'] },
      { label: 'LinkedIn', to: '/', icon: ['fab', 'linkedin-in'] },
      { label: 'Facebook', to: '/', icon: ['fab', 'facebook-f'] },
      { label: 'Pinterest', to: '/', icon: ['fab', 'pinterest-p'] },
      { label: 'Youtube', to: '/', icon: ['fab', 'youtube'] },
      { label: 'Google+', to: '/', icon: ['fab', 'google-plus-g'] },
    ],
  },
  {
    title: 'JOB ALERT',
    links: [{ label: 'Subscribe to our job alert', to: '/' }],
  },
];

/* Reducers */
export default function reducer(state = footerSections) {
  return state;
}
