import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import navLinks from './navLinks';
import footerSections from './footerSections';
import job from './job';

export default combineReducers({
  form: reduxForm,
  navLinks,
  job,
  footerSections,
});
