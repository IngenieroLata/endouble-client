import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SearchPage from './SearchPage';

/**
 * Container to handle header links and responsive logic.
 *
 * @author Diego Toro
 */
class Header extends Component {
  constructor(props) {
    super(props);

    this.state = { menu: false };
    this.openMenu = this.openMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
  }

  renderLinks() {
    return this.props.navLinks.map(({ label, to }) => (
      <li className="nav-item" key={label}>
        <a href={to} className="nav-link">
          {label}
        </a>
      </li>
    ));
  }

  openMenu(e) {
    e.preventDefault();
    this.setState({ menu: true });
  }

  closeMenu(e) {
    e.preventDefault();
    this.setState({ menu: false });
  }

  render() {
    return (
      <div className="bg-black text-white py-3 ">
        <div className="mx-5 relative flex flex-col  xl:flex-row">
          <nav className="flex items-center w-full xl:w-3/5">
            <div className="w-1/6 flex items-center">
              <button
                type="button"
                className="open-menu"
                onClick={this.openMenu}
              >
                <FontAwesomeIcon icon="bars" size="2x" className="text-white" />
              </button>
              <a
                href="/"
                className="text-white no-underline font-yanone text-4xl"
              >
                LOGO
              </a>
            </div>
            <div className="w-5/6">
              <ul className={`nav-menu ${this.state.menu ? 'w-full' : ''}`}>
                <li className="close-menu">
                  <button
                    type="button"
                    className="text-white p-3"
                    onClick={this.closeMenu}
                  >
                    <FontAwesomeIcon icon="times" size="lg" />
                  </button>
                </li>
                {this.renderLinks()}
              </ul>
            </div>
            <div className="absolute pin-r">
              <FontAwesomeIcon icon="suitcase" size="lg" className="mr-3" />
              <span className="text-sm font-bold">1</span>
            </div>
          </nav>
          <div className="mt-2 w-full xl:w-2/5 xl:mr-20">
            <SearchPage />
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  /** Array of navigation links */
  navLinks: PropTypes.arrayOf(
    PropTypes.shape({
      /** Friendly nav-link string*/
      label: PropTypes.string.isRequired,
      /** URL to app content */
      to: PropTypes.string.isRequired,
    })
  ).isRequired,
};

const mapStateToProps = ({ navLinks }) => ({ navLinks });
export default connect(mapStateToProps)(Header);
