import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * Controlled serach bar.
 *
 * @author Diego Toro
 */
class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log(`We should search ${this.state.value}`);
  }

  handleChange({ target }) {
    this.setState({ value: target.value });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="w-full relative">
          <input
            type="search"
            id="search"
            name="search"
            value={this.state.value}
            onChange={this.handleChange}
            className="w-full h-8 p-2 pr-8"
            placeholder="SEARCH FOR JOBS BY KEYWORD"
          />
          <FontAwesomeIcon
            icon="search"
            className="absolute text-grey-dark pin-t pin-r m-2"
          />
        </div>
      </form>
    );
  }
}

export default SearchPage;
