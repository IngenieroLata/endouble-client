import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from './header/Header';
import JobApply from './job/JobApply';
import Footer from './footer/Footer';

/**
 * Container for the entire application.
 *
 * @version 0.0.1
 * @author Diego Toro
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { showBtn: false };
    this.handleScroll = this.handleScroll.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    if (
      document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20
    ) {
      this.setState({ showBtn: true });
    } else {
      this.setState({ showBtn: false });
    }
  }

  handleClick() {
    const scrollDuration = 140;
    const scrollStep = -window.scrollY / (scrollDuration / 15);
    const scrollInterval = setInterval(() => {
      if (window.scrollY != 0) {
        window.scrollBy(0, scrollStep);
      } else {
        clearInterval(scrollInterval);
      }
    }, 15);
  }

  render() {
    return (
      <div>
        <Header />
        <JobApply />
        <Footer />
        <button
          className={`fixed pin-b pin-r bg-white py-3 px-6 text-sm text-grey-darker ${
            this.state.showBtn ? 'block' : 'display-none'
          }`}
          onClick={this.handleClick}
        >
          <span className="display-none xxl:inline-block">BACK TO TOP</span>
          <FontAwesomeIcon icon="caret-up" className="xxl:ml-3" />
        </button>
      </div>
    );
  }
}

export default hot(module)(App);
