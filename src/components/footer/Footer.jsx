import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import FooterSection from './FooterSection';

/**
 * Container to render footer sections
 *
 * @author Diego Toro
 */
const Footer = ({ footerSections }) => {
  return (
    <footer className="bg-white pt-8 mb-8">
      <div className="container py-5 border-t border-solid border-grey-light flex flex-col md:flex-wrap md:flex-row">
        {footerSections.map((section, i) => (
          <FooterSection key={i} {...section} />
        ))}
      </div>
    </footer>
  );
};

Footer.propTypes = {
  /** Array of footer sections */
  footerSections: PropTypes.arrayOf(
    PropTypes.shape({
      /** Friendly section title */
      title: PropTypes.string.isRequired,
      /** Overrides Tailwind CSS default styles */
      sectionClass: PropTypes.string,
      /** Section URL configuration list */
      links: PropTypes.arrayOf(PropTypes.object).isRequired,
    })
  ).isRequired,
};

const mapStateToProps = ({ footerSections }) => ({ footerSections });

export default connect(mapStateToProps)(Footer);
