import React from 'react';
import PropTypes from 'prop-types';

import FooterLink from './FooterLink';

/**
 * Container for footer links
 *
 * @author Diego Toro
 */
const FooterSection = ({ title, sectionClass, links }) => (
  <div className="w-full md:w-1/2 lg:w-1/4" key={title}>
    <h5 className="font-museo font-bold">{title}</h5>
    <ul
      className={`list-reset my-6 ${
        sectionClass ? 'flex flex-col flex-wrap ' + sectionClass : ''
      }`}
    >
      {links.map((link, i) => <FooterLink key={i} {...link} />)}
    </ul>
  </div>
);

FooterSection.propTypes = {
  /** Friendly section title */
  title: PropTypes.string.isRequired,
  /** Overrides Tailwind CSS default styles */
  sectionClass: PropTypes.string,
  /** URL configuration */
  links: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default FooterSection;
