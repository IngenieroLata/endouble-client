import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * Renders a footer link
 *
 * @version 0.0.1
 * @author Diego Toro
 */
const FooterLink = ({ label, to, icon }) => {
  if (icon) {
    return (
      <li className="mb-1">
        <a
          href={to}
          className="flex items-center text-grey-darker no-underline"
        >
          <div className="border border-solid border-grey-darker rounded-full flex items-center justify-center w-6 h-6 mr-3">
            <FontAwesomeIcon
              icon={icon}
              size="xs"
              className="text-grey-darker"
            />
          </div>
          <span>{label}</span>
        </a>
      </li>
    );
  }

  return (
    <li className="mb-3">
      <a href={to} className="text-grey-darker no-underline text-sm">
        {label}
      </a>
    </li>
  );
};

FooterLink.propTypes = {
  /** Friendly text to be shown in the UI */
  label: PropTypes.string.isRequired,
  /** URL to app content */
  to: PropTypes.string.isRequired,
  /** Font Awesome icon config */
  icon: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
};

export default FooterLink;
