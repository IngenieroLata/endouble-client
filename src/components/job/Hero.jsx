import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Container for Hero section
 *
 * @author Diego Toro
 */
class Hero extends Component {
  constructor(props) {
    super(props);
    this.state = { offset: 0 };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    this.setState({ offset: document.documentElement.scrollTop / 7 });
  }

  render() {
    const { height, children, image, parallax } = this.props;
    return (
      <div
        ref={el => (this.hero = el)}
        className={`bg-cover flex flex-col justify-center bg-no-repeat ${height}`}
        style={{
          backgroundImage: `url(${image})`,
          backgroundPositionY: parallax ? `-${this.state.offset}px` : '0',
        }}
      >
        <div className="container">{children}</div>
      </div>
    );
  }
}

Hero.propTypes = {
  /** Overrides Tailwind CSS default styles */
  parallax: PropTypes.bool,
  /** Overrides Tailwind CSS default styles */
  height: PropTypes.string,
  /** Inner react components to be contained */
  children: PropTypes.element,
  /** Background section image */
  image: PropTypes.string.isRequired,
};

Hero.defaultProps = {
  parallax: false,
};

export default Hero;
