import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * Wrapper for textarea controlled by react-redux's input events
 *
 * @author Diego Toro
 */
class TextField extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  render() {
    const {
      input,
      placeholder,
      meta: { touched, valid, error },
    } = this.props;
    return (
      <div className="field-content w-full">
        <div className="control-container">
          <textarea
            placeholder={placeholder}
            className={`control-field py-3 h-32 resize-none ${
              touched && error ? 'border-red' : ''
            } ${valid ? 'border-transparent' : ''}`}
            {...input}
          />
          {touched &&
            valid && (
              <FontAwesomeIcon
                icon="check"
                className="absolute text-blue-darker pin-t pin-r m-3"
              />
            )}
          {touched &&
            error && (
              <FontAwesomeIcon
                icon="times"
                className="absolute text-red pin-t pin-r m-3"
              />
            )}
          {touched && error && <small className="text-red">{error}</small>}
        </div>
      </div>
    );
  }
}

TextField.propTypes = {
  /** Input object provided by react-redux Field */
  input: PropTypes.object.isRequired,
  /** Placeholder for the final textarea */
  placeholder: PropTypes.string,
  /** meta data provided by react-redux Field */
  meta: PropTypes.object.isRequired,
};

export default TextField;
