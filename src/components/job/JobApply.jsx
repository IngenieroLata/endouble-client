import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Hero from './Hero';
import JobForm from './JobForm';
import { applyToJob } from '../../redux/modules/job';
import hero1 from '../../../assets/images/hero_1.png';
import hero2 from '../../../assets/images/hero_2.png';

/**
 * Container for the application form page
 *
 * @author Diego Toro
 */
class JobApply extends Component {
  constructor(props) {
    super(props);
    this.state = { applied: false };
  }
  onSubmit(jobId, formValues) {
    this.props.applyToJob(jobId, formValues);
    this.setState({ applied: true });
  }

  render() {
    const { job } = this.props;
    return (
      <section>
        <Hero parallax image={hero1} height="h-76">
          <div className="text-white text-center font-yanone bg-black-a50 p-8">
            <h1 className="text-6xl">{job.title}</h1>
            <span className="text-4xl font-light">{job.location}</span>
          </div>
        </Hero>
        {!this.state.applied && (
          <JobForm job={job} onSubmit={this.onSubmit.bind(this, job.id)} />
        )}
        {this.state.applied && (
          <div className="container bg-white py-12 text-center">
            <h2 className="font-yanone text-5xl mb-6">
              THANK YOU! {job.title} CANDIDATE
            </h2>
            <p className="font-museo text-lg">
              We will get in touch with you as soon as one of our awesome
              recruiters take a look at your awesome profile!.
            </p>
          </div>
        )}
        <Hero image={hero2} height="h-auto lg:h-100">
          <div className="my-8 bg-white flex flex-col lg:flex-row lg:my-0">
            <div className="w-full lg:w-1/2 p-8">
              <h2 className="font-yanone text-5xl mb-8">
                APPLICATION PROCEDURE
              </h2>
              <ul className="align-list">
                <li>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                </li>
                <li>
                  Numquam aspernatur dolore quae? Ducimus dicta earum numquam.
                </li>
                <li>
                  Repellat exercitationem ex doloribus molestiae iusto neque
                  aliquid.
                </li>
                <li>
                  Placeat, dignissimos doloremque inventore vero repudiandae.
                </li>
              </ul>
              <a href="" className="text-grey-darker no-underline text-sm">
                Continue reading
              </a>
            </div>
            <div className="w-full lg:w-1/2 p-8">
              <h2 className="font-yanone text-5xl mb-8">GOT A QUESTION?</h2>
              <p className="font-museo">Please contact recruitment</p>
              <br />
              <p>T. +31 20 123 456 78</p>
              <p>
                Or{' '}
                <a href="" className="text-grey-darker">
                  send us an email
                </a>
              </p>
            </div>
          </div>
        </Hero>
      </section>
    );
  }
}

JobApply.propTypes = {
  /** Job configuration */
  job: PropTypes.shape({
    /** Id to performe action on backend side */
    id: PropTypes.string.isRequired,
    /** Friendly job title */
    title: PropTypes.string.isRequired,
    /** Friendly location string */
    location: PropTypes.string.isRequired,
    /** Application form configuration */
    formFields: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
  /** Action Creator to handle the form submission */
  applyToJob: PropTypes.func.isRequired,
};

const mapStateToProps = ({ job }) => ({ job });

export default connect(
  mapStateToProps,
  { applyToJob }
)(JobApply);
