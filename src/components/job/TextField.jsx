import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * Wrapper for generic input controlled by react-redux's input events
 *
 * @author Diego Toro
 */
const TextField = ({
  input,
  type,
  placeholder,
  meta: { touched, valid, error },
}) => (
  <div className="field-content">
    <div className="control-container">
      <input
        type={type}
        placeholder={placeholder}
        className={`control-field ${touched && error ? 'border-red' : ''} ${
          valid ? 'border-transparent' : ''
        }`}
        {...input}
      />
      {touched &&
        valid && (
          <FontAwesomeIcon
            icon="check"
            className="absolute text-blue-darker pin-t pin-r m-3"
          />
        )}
      {touched &&
        error && (
          <FontAwesomeIcon
            icon="times"
            className="absolute text-red pin-t pin-r m-3"
          />
        )}
      {touched && error && <small className="text-red">{error}</small>}
    </div>
  </div>
);

TextField.propTypes = {
  /** Input object provided by react-redux Field */
  input: PropTypes.object.isRequired,
  /** Input type to HTML input tag */
  type: PropTypes.string.isRequired,
  /** Placeholder for the final input */
  placeholder: PropTypes.string,
  /** meta data provided by react-redux Field */
  meta: PropTypes.object.isRequired,
};

export default TextField;
