import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * Wrapper for select-options controlled by react-redux's input events
 *
 * @author Diego Toro
 */
class SelectField extends Component {
  renderOptions() {
    const { values } = this.props;
    return values.map(({ value, label }) => (
      <option key={value} value={value}>
        {label}
      </option>
    ));
  }

  render() {
    const {
      input,
      placeholder,
      meta: { touched, error },
    } = this.props;

    return (
      <div className="field-content">
        <div className="control-container">
          <select
            className={`control-field appearance-none bg-white rounded-none ${
              touched && error ? 'border-red text-red' : ''
            }`}
            {...input}
          >
            >
            <option value="">{placeholder}</option>
            {this.renderOptions()}
          </select>
          <FontAwesomeIcon
            icon="chevron-down"
            size="sm"
            className={`chevron-select ${touched && error ? 'text-red' : ''}`}
          />
          {touched && error && <small className="text-red">{error}</small>}
        </div>
      </div>
    );
  }
}

SelectField.propTypes = {
  /** Input object provided by react-redux Field */
  input: PropTypes.object.isRequired,
  /** Placeholder for no-value option */
  placeholder: PropTypes.string,
  /** meta data provided by react-redux Field */
  meta: PropTypes.object.isRequired,
};

export default SelectField;
