import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';

import TextField from './TextField';
import DateField from './DateField';
import TextAreaField from './TextAreaField';
import SelectField from './SelectField';
import FileField from './FileField';
import * as validators from '../../utils/validators';

/**
 * Render form fields by configuration.
 *
 * @param {Object[]} formFields - Field list of configuration.
 * @param {string} formFields[].name - Name of the form field.
 * @param {string} formFields[].type - Type of the form field.
 * @param {string} [formFields[].placeholder] - Placeholder for the form field.
 * @param {(string|string[])} [formFields[].validation] -Validations for the form field.
 * @returns {Field[]} - An Array of Field wrapper with custom form fields.
 */
const renderFields = formFields => {
  return formFields.map(({ validation, ...field }) => {
    const validate = (validation || []).map(key => validators[key]);
    switch (field.type) {
      case 'date':
        return (
          <Field
            key={field.name}
            {...field}
            component={DateField}
            validate={validate}
          />
        );
      case 'select':
        return (
          <Field
            key={field.name}
            {...field}
            component={SelectField}
            validate={validate}
          />
        );
      default:
        return (
          <Field
            key={field.name}
            {...field}
            component={TextField}
            validate={validate}
          />
        );
    }
  });
};

/**
 * Container for Form inputs and handle submission
 *
 * @author Diego Toro
 */
const JobForm = ({ job, onSubmit, handleSubmit }) => (
  <div className="container py-12 px-4 md:px-0">
    <h2 className="font-yanone text-5xl mb-12">
      APPLY FOR THE POSITION OF {job.title}
    </h2>
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset className="mb-5">
        <legend className="font-museo font-bold mb-3">
          Personal Details *
        </legend>
        <div className="flex flex-col lg:flex-row lg:flex-wrap ">
          {renderFields(job.formFields)}
        </div>
      </fieldset>

      <div className="flex flex-col lg:flex-row">
        <fieldset className="w-full lg:w-1/2 mb-8">
          <legend className="font-museo font-bold mb-3">
            Your Motivation *
          </legend>
          <div className="flex flex-col lg:flex-row lg:flex-wrap ">
            <Field
              name="motivation"
              type="textarea"
              placeholder="Start typing your motivational letter here..."
              component={TextAreaField}
              validate={validators.required}
            />
          </div>
        </fieldset>

        <fieldset className="w-full lg:w-1/2 mb-8">
          <legend className="font-museo font-bold mb-3">
            Attach your documents (.doc(x), .pdf, .rtf or .txt)
          </legend>
          <div className="flex flex-col lg:-mr-4 lg:flex-row lg:flex-wrap ">
            <div className="field-content w-full">
              <Field
                icons
                name="resume"
                label="Resume *"
                component={FileField}
                validate={validators.required}
              />
              <Field name="portfolio" label="Portfolio" component={FileField} />
            </div>
          </div>
        </fieldset>
      </div>

      <div className="flex flex-col lg:flex-row-reverse">
        <div className="w-full mb-8 lg:w-1/2 flex items-center justify-between">
          <div className="flex items-center">
            <Field
              id="sendCopy"
              name="sendCopy"
              component="input"
              type="checkbox"
              className="mr-2"
            />
            <label
              htmlFor="sendCopy"
              className="font-museo text-sm text-grey-darker"
            >
              Send me a copy
            </label>
          </div>
          <button
            type="submit"
            className="w-2/3 bg-blue-dark py-3 text-white font-museo font-bold"
          >
            APPLY FOR THIS JOB
          </button>
        </div>
        <div className="w-full text-center lg:text-left lg:w-1/2">
          <a href="" className="text-grey-darker no-underline text-sm">
            &lt;&nbsp;&nbsp;&nbsp;Back to job description
          </a>
        </div>
      </div>
    </form>
  </div>
);

JobForm.propTypes = {
  /** Job configuration */
  job: PropTypes.shape({
    /** Id to performe action on backend side */
    id: PropTypes.string.isRequired,
    /** Friendly job title */
    title: PropTypes.string.isRequired,
    /** Friendly location string */
    location: PropTypes.string.isRequired,
    /** Application form configuration */
    formFields: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
  /** Function provided by some container to external event handler */
  onSubmit: PropTypes.func.isRequired,
  /** Function provided by reduxForm to handle the form submission */
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'jobForm',
})(JobForm);
