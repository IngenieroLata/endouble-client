import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DropboxChooser from 'react-dropbox-chooser';

import keys from '../../config/keys';

/**
 * Wrapper for file uploader with DropboxCooser controlled by react-redux's input events
 *
 * @author Diego Toro
 */
class FileField extends Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false };
  }

  handleChange(handler) {
    return e => {
      const { files } = e.target;
      if (files.length) {
        handler({ file: files[0], name: files[0].name });
        this.setState({ loaded: true });
      } else {
        handler();
        this.setState({ loaded: false });
      }
    };
  }

  render() {
    const {
      input: { onChange, onBlur, value: omitValue, ...inputProps },
      meta: { touched, valid, error },
      label,
      dropbox,
      icons,
    } = this.props;

    return (
      <div className="control-container flex flex-wrap items-center">
        <label
          className={`font-museo font-bold w-1/3 flex pr-3 justify-between ${
            touched && error ? 'text-red' : ''
          }`}
        >
          <span>{label}</span>
          {icons && (
            <span>
              {this.state.loaded && valid && <FontAwesomeIcon icon="check" />}
              {touched &&
                error && <FontAwesomeIcon icon="times" className="text-red" />}
            </span>
          )}
        </label>
        <div className={dropbox ? 'w-1/3 pr-1' : 'w-2/3'}>
          <input
            type="file"
            accept=".doc,.docx,.pdf,.rtf,.txt"
            id={inputProps.name}
            {...inputProps}
            onChange={this.handleChange(onChange)}
          />
          <label
            htmlFor={inputProps.name}
            className="text-white text-center bg-blue-dark py-3n font-museo block flex flex-col items-center sm:flex-row sm:justify-center"
          >
            <FontAwesomeIcon icon="desktop" className="mb-1 sm:mr-3 sm:mb-0" />
            <span>UPLOAD</span>
          </label>
        </div>
        {dropbox && (
          <div className="w-1/3 pl-1">
            <DropboxChooser
              appKey={keys.DROPBOX_KEY}
              success={files => onChange(files)}
              extensions={['.doc', '.docx', '.pdf', '.rtf', '.txt']}
            >
              <div className="text-white text-center bg-blue-dark py-3 font-museo block flex flex-col items-center sm:flex-row sm:justify-center">
                <FontAwesomeIcon
                  icon={['fab', 'dropbox']}
                  size="lg"
                  className="sm:mr-3"
                />
                DROPBOX
              </div>
            </DropboxChooser>
          </div>
        )}
        {touched && error && <small className="text-red">{error}</small>}
      </div>
    );
  }
}

FileField.propTypes = {
  /** Input object provided by react-redux Field */
  input: PropTypes.object.isRequired,
  /** meta data provided by react-redux Field */
  meta: PropTypes.object.isRequired,
  /** Label to be shown */
  label: PropTypes.string.isRequired,
  /** Boolean: to allow Dropbox file selection */
  dropbox: PropTypes.bool,
  /** Boolean: to allow validation icons */
  icons: PropTypes.bool,
};

FileField.defaultProps = {
  dropbox: true,
  icons: false,
};

export default FileField;
