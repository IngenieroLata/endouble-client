import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DatePicker from 'react-datepicker';

/**
 * Wrapper for DatePicker component controlled by react-redux's input events
 *
 * @author Diego Toro
 */
const DateField = ({ input, placeholder, meta: { touched, valid, error } }) => {
  const { value, onBlur, ...inputValues } = input;
  return (
    <div className="field-content">
      <div className="control-container">
        <DatePicker
          placeholderText={placeholder}
          className={`control-field ${touched && error ? 'border-red' : ''} ${
            valid ? 'border-transparent' : ''
          }`}
          selected={typeof value === 'object' ? value : null}
          onBlur={() => input.onBlur()}
          {...inputValues}
        />
        {touched &&
          valid && (
            <FontAwesomeIcon
              icon="check"
              className="absolute text-blue-darker pin-t pin-r m-3"
            />
          )}
        {touched &&
          error && (
            <FontAwesomeIcon
              icon="times"
              className="absolute text-red pin-t pin-r m-3"
            />
          )}
        {touched && error && <small className="text-red">{error}</small>}
      </div>
    </div>
  );
};

DateField.propTypes = {
  /** Input object provided by react-redux Field */
  input: PropTypes.object.isRequired,
  /** Placeholder for the final input */
  placeholder: PropTypes.string,
  /** meta data provided by react-redux Field */
  meta: PropTypes.object.isRequired,
};

export default DateField;
