import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEnvelope,
  faCaretUp,
  faSuitcase,
  faSearch,
  faTimes,
  faBars,
  faCheck,
  faChevronDown,
  faDesktop,
} from '@fortawesome/free-solid-svg-icons';
import {
  faInstagram,
  faTwitter,
  faTumblr,
  faLinkedinIn,
  faFacebookF,
  faPinterestP,
  faYoutube,
  faGooglePlusG,
  faDropbox,
} from '@fortawesome/free-brands-svg-icons';

library.add(
  faEnvelope,
  faCaretUp,
  faSuitcase,
  faSearch,
  faTimes,
  faBars,
  faCheck,
  faChevronDown,
  faDesktop
);
library.add(
  faInstagram,
  faTwitter,
  faTumblr,
  faLinkedinIn,
  faFacebookF,
  faPinterestP,
  faYoutube,
  faGooglePlusG,
  faDropbox
);
